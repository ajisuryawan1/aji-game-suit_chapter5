const express = require("express");
const router = express.Router();
const user = require("./user.json");

router.get("/", (req, res) => {
    res.render("index");
});

router.get("/game", (req, res) => {
    res.render("game");
});

router.get("/login", (req, res) => {
    res.render("login");
});

router.post("/login", (req, res) => {
    const { email, password } = req.body;

    if (user.email === email && user.password === password) {
        res.redirect("/game");
    } else {
        res.redirect("/login");
    }
});

module.exports = router;
