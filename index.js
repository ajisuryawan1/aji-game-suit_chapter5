/* const os = require("os");
const luasSegitiga = require("./assets/segitiga")
const fs = require("fs");
const http = require("http"); */
const express = require("express");
const app = express();
const port = 8000;
const router = require("./router");

/*
console.log("hello world");
console.log("free memory: ", os.freemem());
console.log(luasSegitiga(3, 6));
*/

// read file
/* const isi = fs.readFileSync("./assets/text.txt", "utf-8");
console.log(isi); */

// write file
// fs.writeFileSync("./assets/hai.txt", "hai")

// write file json
/* const createPerson = (person) => {
    fs.writeFileSync("./assets/person.json", JSON.stringify(person));
    return person;
} 

const hasan = createPerson({
    name: "hasan", 
    age: 18, 
    address: "bumi",
});

console.log(hasan); */

// read file json
/* const person = require("./assets/person.json");
console.log(person);

create server html
function onRequest(request, response) {
    response.writeHead(200, {"content-Type": "text/html"});
    fs.readFile("./index.html", null, (error, data) => {
        if (error) {
            response.writeHead(404);
            response.write("file note found");
        } else {
            response.write(data);
        }
        response.end();
    });
} */

// create server json
/* function onRequest(request, response) {
    response.writeHead(200, {"content-Type": "application/json"});
    fs.readFile("./assets/person.json", null, (error, data) => {
        if (error) {
            response.writeHead(404);
            response.write("file note found");
        } else {
            response.write(data);
        }
        response.end();
    });
} */

// http.createServer(onRequest).listen(8000);

const logger = (req, res, next) => {
    console.log(`${req.method} ${req.url}`);
    next();
}

app.use(logger);
app.use(express.urlencoded({ extended: false }));
app.use(router);
app.use(express.static(`${__dirname}/assets`));
app.set("view engine", "ejs");

// internal server error handler
app.use((err, req, res, next) => {
    console.log(err);
    res.status(500).json({
        status: "fail",
        errors: err.message,
    });
});

// 404 handler
app.use((req, res, next) => {
    res.status(404).json({
        status: "fail",
        errors: "are you lost?",
    });
});

app.listen(port, () => console.log(`server nyala http://localhost:${port}`));
